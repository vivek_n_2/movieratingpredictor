#! /usr/bin/env python

import re
from numpy import partition
import pickle
import pandas as pd

moviedata = {}

def get_ratings(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            splitline = line.strip('\n').split()
            if len(splitline)>4:
                if len(line)>6 and ''.join(line[:6].split())=='':
                    votes = int(splitline[1])
                    rating = float(splitline[2])
                    movie = ' '.join(splitline[3:])
                    #print movie,votes,rating
                    if movie not in moviedata:
                        moviedata[movie] = {'rating':rating}
                    else:
                        moviedata[movie]['rating']=rating
        linenum += 1
    fptr.close()

def get_genres(filename, startline):
    fptr = open(filename,'rw+')
    linenum = 1
    prevmovie = None
    prevgenre = None
    for line in fptr.readlines():
        if linenum>=startline:
            splitline = line.strip('\n').split()
            if len(splitline)>2:
                movie = ' '.join(splitline[:-1])
                genre = splitline[-1]
                if prevmovie is None:
                    prevmovie = movie
                    prevgenre = genre
                elif prevmovie == movie:
                    prevgenre += "|"+genre
                else:
                    if prevmovie not in moviedata:
                        moviedata[prevmovie] = {'genre':prevgenre}
                    else:
                        moviedata[prevmovie]['genre']=prevgenre
                    prevmovie = movie
                    prevgenre = genre
        linenum += 1
    if prevmovie is not None:
        if prevmovie not in moviedata:
            moviedata[prevmovie] = {'genre':prevgenre}
        else:
            moviedata[prevmovie]['genre']=prevgenre
    fptr.close()

def get_directors(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    director = "$$"
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            if line.strip()=="-"*77:
                break
            if line.strip()=="":
                flag=0
                continue
            if(flag==0):
                flag=1
                temp = line.split('\t')
                director=temp[0].strip()
                movie=temp[-1].strip()
                if movie not in moviedata:
                    moviedata[movie]={'director':director}
                else:
                    moviedata[movie]['director'] = director
            elif(flag==1):
                movie = line.strip('\t').strip()
                if movie not in moviedata:
                    moviedata[movie]={'director':director}
                else:
                    moviedata[movie]['director'] = director
        linenum+=1
    fptr.close()

def get_language(filename, startline):
    fptr = open(filename,'rw+')
    linenum = 1
    for line in fptr.readlines():
        if linenum>=startline:
            splitline = line.strip('\n').split()
            if len(splitline)>=2:
                temp = line.split('}')
                if len(temp)>1:
                    movie=temp[0].strip()
                    lang=temp[1].strip()
                else:
                    temp=line.split(')')
                    lang = ""
                    for i in temp[1:]:
                        lang+=i
                    movie=temp[0].strip()+")"
                    lang=lang.strip()
                    if(len(temp)>2):
                        lang=lang+")"
                print movie,lang
        linenum+=1
    fptr.close()

def get_color_info(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    color = "Color"
    bnw = "Black and White"
    for line in fptr.readlines():
        if linenum>endline:
            break
        if linenum>=startline and linenum<=endline:
            array=line.strip().split('\t')
            col=""
            if color in line:
                col = line[line.find(color):].strip()
            elif bnw in line:
                col = line[line.find(bnw):].strip()
            movie=line[:line.find(col)].strip()
            #print movie,"->",col
            if movie not in moviedata:
                moviedata[movie]={'color-info':col}
            else:
                moviedata[movie]['color-info'] = col
        linenum+=1
    fptr.close()

def get_time(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            temp = line.split("\t")
            runt=""
            for i in temp[1:]:
                runt+=i
            runt=runt.strip()
            if ":" in runt:
                runt = runt[runt.find(":")+1:]
            movie = temp[0].strip()
            #print movie,"->",runt
            if runt.isdigit():
                runt=int(runt)
                if movie not in moviedata:
                    moviedata[movie]={'runtime':runt}
                else:
                    moviedata[movie]['runtime'] = runt
            elif movie not in moviedata:
                moviedata[movie]={}
        linenum+=1
    fptr.close()

def get_production(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            temp = line.split("\t")
            prod=""
            for i in temp[1:]:
                prod+=i
            prod=prod.strip()
            movie = temp[0].strip()
            if movie not in moviedata:
                moviedata[movie]={'production':[prod,]}
            elif 'production' in moviedata[movie]:
                moviedata[movie]['production'].append(prod)
            else:
                moviedata[movie]['production']=[prod,]
            #print movie,"->",prod
        linenum+=1
    fptr.close()

def get_countries(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            array = line.split("\t")
            country = array[-1].strip()
            movie = line[:line.find(country)].strip()
            #print movie,"->",country
            if movie not in moviedata:
                moviedata[movie]={'country':[country,]}
            elif 'country' in moviedata[movie]:
                moviedata[movie]['country'].append(country)
            else:
                moviedata[movie]['country']=[country,]
        linenum+=1
    fptr.close()
        
def impute():
    # Filling missing ratings
    tempratings = [moviedata[movie]['rating'] for movie in moviedata if 'rating' in moviedata[movie]]
    meanrating = sum(tempratings)/len(tempratings)
    for movie in moviedata:
        if 'rating' not in moviedata[movie]:
            moviedata[movie]['rating'] = meanrating
    del tempratings[:]
    del tempratings
    # converting genre to numeric and filling missing genres
    tempgenres = {}
    for movie in moviedata:
        if 'genre' in moviedata[movie]:
            movierating = moviedata[movie]['rating']
            for genre in moviedata[movie]['genre'].split('|'):
                if genre not in tempgenres:
                    tempgenres[genre] = [movierating,1]
                else:
                    tempgenres[genre][0] += movierating
                    tempgenres[genre][1] += 1
    for genre in tempgenres:
        tsum,tcnt = tempgenres[genre]
        tempgenres[genre] = tsum/tcnt
    for movie in moviedata:
        if 'genre' in moviedata[movie]:
            genrevals = [tempgenres[genre] for genre in moviedata[movie]['genre'].split('|')]
            moviedata[movie]['genre'] = sum(genrevals)/len(genrevals)
            del genrevals[:]
            del genrevals
    tempgenres.clear()
    del tempgenres
    tempgenres = [moviedata[movie]['genre'] for movie in moviedata if 'genre' in moviedata[movie]]
    meangenre = sum(tempgenres)/len(tempgenres)
    for movie in moviedata:
        if 'genre' not in moviedata[movie]:
            moviedata[movie]['genre'] = meangenre
    del tempgenres[:]
    del tempgenres
    # converting director to numeric and filling missing directors
    tempdirectors = {}
    for movie in moviedata:
        if 'director' in moviedata[movie]:
            movierating = moviedata[movie]['rating']
            director = moviedata[movie]['director']
            if director not in tempdirectors:
                    tempdirectors[director] = [movierating,1]
            else:
                tempdirectors[director][0] += movierating
                tempdirectors[director][1] += 1
    for director in tempdirectors:
        tsum,tcnt = tempdirectors[director]
        tempdirectors[director] = tsum/tcnt
    for movie in moviedata:
        if 'director' in moviedata[movie]:
            director = tempdirectors[moviedata[movie]['director']]
            moviedata[movie]['director'] = director
    tempdirectors.clear()
    del tempdirectors
    tempdirectors = [moviedata[movie]['director'] for movie in moviedata if 'director' in moviedata[movie]]
    meandirector = sum(tempdirectors)/len(tempdirectors)
    for movie in moviedata:
        if 'director' not in moviedata[movie]:
            moviedata[movie]['director'] = meandirector
    del tempdirectors[:]
    del tempdirectors
    # converting colorinfo to numeric and filling missing informations
    tempcolors = {}
    for movie in moviedata:
        if 'color-info' in moviedata[movie]:
            movierating = moviedata[movie]['rating']
            color = moviedata[movie]['color-info']
            if color not in tempcolors:
                    tempcolors[color] = [movierating,1]
            else:
                tempcolors[color][0] += movierating
                tempcolors[color][1] += 1
    for color in tempcolors:
        tsum,tcnt = tempcolors[color]
        tempcolors[color] = tsum/tcnt
    for movie in moviedata:
        if 'color-info' in moviedata[movie]:
            color = tempcolors[moviedata[movie]['color-info']]
            moviedata[movie]['color-info'] = color
    tempcolors.clear()
    del tempcolors
    tempcolors = [moviedata[movie]['color-info'] for movie in moviedata if 'color-info' in moviedata[movie]]
    meancolor = sum(tempcolors)/len(tempcolors)
    for movie in moviedata:
        if 'color-info' not in moviedata[movie]:
            moviedata[movie]['color-info'] = meancolor
    del tempcolors[:]
    del tempcolors
    # Filling missing runtimes
    tempruntimes = [moviedata[movie]['runtime'] for movie in moviedata if 'runtime' in moviedata[movie]]
    meanruntime = sum(tempruntimes)/len(tempruntimes)
    for movie in moviedata:
        if 'runtime' not in moviedata[movie]:
            moviedata[movie]['runtime'] = meanruntime
    del tempruntimes[:]
    del tempruntimes
    # converting productions to numeric and filling missing productions
    tempproductions = {}
    for movie in moviedata:
        if 'production' in moviedata[movie]:
            movierating = moviedata[movie]['rating']
            for production in moviedata[movie]['production']:
                if production not in tempproductions:
                    tempproductions[production] = [movierating,1]
                else:
                    tempproductions[production][0] += movierating
                    tempproductions[production][1] += 1
    for production in tempproductions:
        tsum,tcnt = tempproductions[production]
        tempproductions[production] = tsum/tcnt
    for movie in moviedata:
        if 'production' in moviedata[movie]:
            productionvals = [tempproductions[production] for production in moviedata[movie]['production']]
            moviedata[movie]['production'] = sum(productionvals)/len(productionvals)
    tempproductions.clear()
    del tempproductions
    tempproductions = [moviedata[movie]['production'] for movie in moviedata if 'production' in moviedata[movie]]
    meanproduction = sum(tempproductions)/len(tempproductions)
    for movie in moviedata:
        if 'production' not in moviedata[movie]:
            moviedata[movie]['production'] = meanproduction
    del tempproductions[:]
    del tempproductions
    
    # converting countries to numeric and filling missing countries
    tempcountries = {}
    for movie in moviedata:
        if 'country' in moviedata[movie]:
            movierating = moviedata[movie]['rating']
            for country in moviedata[movie]['country']:
                if country not in tempcountries:
                    tempcountries[country] = [movierating,]
                else:
                    tempcountries[country].append(movierating)
    for country in tempcountries:
        ind34 = (3*len(tempcountries[country]))/4
        elem34 = partition(tempcountries[country],ind34)[ind34]
        tempcountries[country] = elem34
    for movie in moviedata:
        if 'country' in moviedata[movie]:
            countryvals = [tempcountries[country] for country in moviedata[movie]['country']]
            ind34 = (3*len(countryvals))/4
            elem34 = partition(countryvals,ind34)[ind34]
            moviedata[movie]['country'] = elem34
    tempcountries = [moviedata[movie]['country'] for movie in moviedata if 'country' in moviedata[movie]]
    meancountry = sum(tempcountries)/len(tempcountries)
    for movie in moviedata:
        if 'country' not in moviedata[movie]:
            moviedata[movie]['country'] = meancountry
    print meanrating,meangenre,meandirector,meancolor,meanruntime,meanproduction,meancountry
    """
    final_movies = {}
    for key in moviedata:
        temp = "'"+key+"'"
        tkey = temp.replace('"','').strip()
        if tkey!='':
            final_movies[tkey]=moviedata[key]
    with open('movie_dump.pickle', 'wb') as handle: #pickle
          pickle.dump(moviedata, handle)
    """
    pd.DataFrame(moviedata).T.reset_index().to_csv('final_movies.csv', header=True, index=False)
    print meanrating,meangenre,meandirector,meancolor,meanruntime,meanproduction

get_ratings('../RawData/ratings.list',29,686589)
get_genres('../RawData/genres.list',383)
get_directors('../RawData/directors.list', 236, 3082777)
get_color_info('../RawData/color-info.list', 15, 1907159)
get_time('../RawData/running-times.list', 15, 1331161)
get_production('../RawData/production-companies.list', 15, 2555536)
get_countries('../RawData/countries.list', 15, 1960739)
impute()
#get_language('../RawData/language.list',15)
