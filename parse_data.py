#! /usr/bin/env python

import re

def get_genres(filename, startline):
    fptr = open(filename,'rw+')
    linenum = 1
    prevmovie = None
    prevgenre = None
    for line in fptr.readlines():
        if linenum>=startline:
            splitline = line.strip('\n').split()
            if len(splitline)>2:
                movie = ' '.join(splitline[:-1])
                genre = splitline[-1]
                if prevmovie is None:
                    prevmovie = movie
                    prevgenre = genre
                elif prevmovie == movie:
                    prevgenre += "|"+genre
                else:
                    print prevmovie,prevgenre
                    prevmovie = movie
                    prevgenre = genre
        linenum += 1
    if prevmovie is not None:
        print prevmovie,prevgenre

def get_ratings(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            splitline = line.strip('\n').split()
            if len(splitline)>4:
                if len(line)>6 and ''.join(line[:6].split())=='':
                    votes = int(splitline[1])
                    rating = float(splitline[2])
                    movie = ' '.join(splitline[3:])
                    print movie,votes,rating
        linenum += 1

def get_directors(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    director = "$$"
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            if line.strip()=="-"*77:
                break
            if line.strip()=="":
                flag=0
                continue
            if(flag==0):
                flag=1
                temp = line.split('\t')
                director=temp[0].strip()
                movie=temp[-1].strip()
                print movie,director
            elif(flag==1):
                movie = line.strip('\t').strip()
                print movie,director
        linenum+=1

def get_language(filename, startline):
    fptr = open(filename,'rw+')
    linenum = 1
    for line in fptr.readlines():
        if linenum>=startline:
            splitline = line.strip('\n').split()
            if len(splitline)>=2:
                temp = line.split('}')
                if len(temp)>1:
                    movie=temp[0].strip()
                    lang=temp[1].strip()
                else:
                    temp=line.split(')')
                    lang = ""
                    for i in temp[1:]:
                        lang+=i
                    movie=temp[0].strip()+")"
                    lang=lang.strip()
                    if(len(temp)>2):
                        lang=lang+")"
                print movie,lang
        linenum+=1

def get_color_info(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    color = "Color"
    bnw = "Black and White"
    for line in fptr.readlines():
        if linenum>endline:
            break
        if linenum>=startline and linenum<=endline:
            array=line.strip().split('\t')
            col=""
            if color in line:
                col = line[line.find(color):].strip()
            elif bnw in line:
                col = line[line.find(bnw):].strip()
            movie=line[:line.find(col)].strip()
            print movie,"->",col
        linenum+=1

def get_countries(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            array = line.split("\t")
            country = array[-1].strip()
            movie = line[:line.find(country)].strip()
            print movie,"->",country
        linenum+=1

def get_lang(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            line = line.strip()
            array = line.split("\t")
            if line[-1]==")":
                temp=-1
                for i in range(len(line)-1,0,-1):
                    if line[i]=="(":
                        temp=i
                        break
                nline = line[:temp].strip()
                array=nline.split("\t")
                lang = array[-1]+line[temp:]
                movie = ""
                for i in array[:-1]:
                    movie+=i
            else:
                lang=array[-1]
                movie=""
                for i in array[:-1]:
                    movie+=i
            print movie,"->",lang
        linenum+=1

def get_production(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            temp = line.split("\t")
            prod=""
            for i in temp[1:]:
                prod+=i
            prod=prod.strip()
            movie = temp[0].strip()
            print movie,"->",prod
        linenum+=1

def get_time(filename, startline, endline):
    fptr = open(filename,'rw+')
    linenum = 1
    flag = 0
    for line in fptr.readlines():
        if linenum>=startline and linenum<=endline:
            temp = line.split("\t")
            runt=""
            for i in temp[1:]:
                runt+=i
            runt=runt.strip()
            if ":" in runt:
                runt = runt[runt.find(":")+1:]
            movie = temp[0].strip()
            print movie,"->",runt
        linenum+=1

get_lang('../RawData/language.list', 15, 1931074)
"""
get_time('../RawData/running-times.list', 15, 1331161)
get_production('../RawData/production-companies.list', 15, 2555536)
get_lang('../RawData/language.list', 15, 1931074)
get_countries('../RawData/countries.list', 15, 1960739)
get_color_info('../RawData/color-info.list', 15, 1907159)
get_language('../RawData/language.list',15)
get_directors('../RawData/directors.list', 236, 3082777)
get_genres('../RawData/genres.list',383)
get_ratings('../RawData/ratings.list',29,686589)
"""
