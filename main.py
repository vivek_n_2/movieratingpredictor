import pandas as pd

from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier

from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn import metrics

data = pd.read_csv('../tfm.csv')
train, test = train_test_split(data, test_size = 0.2)
train = train.drop('index',axis=1)
test = test.drop('index',axis=1)
for index,row in train.iterrows():
    if row['rating']>=7.5:
        row['rating']=4
    elif row['rating']>=5.0:
        row['rating']=3
    elif row['rating']>=2.5:
        row['rating']=2
    else:
        row['rating']=1

target = train.rating
train = train.drop('rating',axis=1)

    # K-Nearest negihbours
clf = KNeighborsClassifier(n_neighbors=3)
    # Decision Tree Classifier
#clf = DecisionTreeClassifier(random_state=0)

clf.fit(train, target)
predicted = cross_val_predict(clf, train, target, cv=10)
print metrics.accuracy_score(target, predicted)


data = data.drop('index',axis=1)
for index,row in data.iterrows():
    if row['rating']>=7.5:
        row['rating']=4
    elif row['rating']>=5.0:
        row['rating']=3
    elif row['rating']>=2.5:
        row['rating']=2
    else:
        row['rating']=1
target = data.rating
data = data.drop('rating',axis=1)

X_train, X_test, y_train, y_test = train_test_split(data, target, random_state=0,test_size=0.2)
clf = KNeighborsClassifier(n_neighbors=3)
y_pred = clf.fit(X_train, y_train).predict(X_test)
print confusion_matrix(y_test, y_pred)
