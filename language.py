#! /usr/bin/env python

def get_language(filename, startline):
    fptr = open(filename,'rw+')
    linenum = 1
    for line in fptr.readlines():
        if linenum>=startline:
            splitline = line.strip('\n').split()
            if len(splitline)>=2:
                temp = line.split('}')
                if len(temp)>1:
                    movie=temp[0].strip()
                    lang=temp[1].strip()
                else:
                    temp=line.split(')')
                    lang = ""
                    for i in temp[1:]:
                        lang+=i
                    movie=temp[0].strip()+")"
                    lang=lang.strip()
                    if(len(temp)>2):
                        lang=lang+")"
                print movie,lang
        linenum+=1

get_language('../IMDB_Dataset/language.list',15)
#get_language('../IMDB_Dataset/temp_lang.list',1)
